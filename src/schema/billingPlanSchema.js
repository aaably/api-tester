export default {
    'type': 'object',
    'required': ['id', 'name', 'duration', 'amount', 'message', 'currency', 'type'],
    'properties': {
        'id' : {
            'type': 'string'
        },
        'name': {
            'type': 'string'
        },
        'duration': {
            'type': 'number'
        },
        'amount': {
            'type': 'object',
            'required': ['saving', 'monthly', 'total'],
            'properties': {
                'saving': {
                    'type': 'string'
                },
                'monthly': {
                    'type': 'string'
                },
                'total': {
                    'type': 'string'
                }
            }
        },
        'message': {
            'type': ['string', 'null']
        },
        'currency': {
            'enum': ['PLN', 'EUR']
        },
        'type': {
            'enum': ['paid subscription']
        }
    }
};