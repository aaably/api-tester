export default {
    'type': 'object',
    'required': ['isReviewed'],
    'properties': {
        'isReviewed' : {
            'type': 'boolean'
        }
    }
};