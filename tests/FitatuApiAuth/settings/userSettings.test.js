import {expect} from 'chai';
import CONFIG from '../../../config/config';
import ApiClient from '../../../src/ApiClient';
import momentRandom from 'moment-random';
import _ from 'lodash';

let payload;

describe('User Settings', () => {

    Object.keys(CONFIG.LOCALES).forEach(locale => {
        let localeData = CONFIG.LOCALES[locale];

        describe(locale, () => {
            let Client = new ApiClient({locale});

            describe('Read user settings', () => {
                /* test */
                it(`reads REGULAR USER settings`, (done) => {
                    Client
                        .authorize()
                        .then((Client) => {
                            Client.get(`/users/${Client.$user.$id}/settings`)
                                .then(response => {
                                    let keys = Object.keys(response.data);
                                    let expectedKeys = [
                                        'userId', 'sex', 'birthDate', 'height', 'heightUnit',
                                        'heightCm', 'weightStart', 'weightStartUnit', 'weightStartKg',
                                        'weightCurrent', 'weightCurrentUnit', 'weightCurrentKg', 'weightTarget',
                                        'weightTargetUnit', 'weightTargetKg', 'age', 'bmr'
                                    ];

                                    // expectations
                                    expect(response.status).to.equal(200);
                                    expectedKeys.forEach((expectedKey) => {
                                        expect(keys).to.include(expectedKey);
                                    })
                                })
                                .catch((err) => {
                                    throw new Error(err.response.data.message)
                                })
                                .then(done, done);
                        })
                });

                if (localeData.PREMIUM_AVAILABLE) {
                    /* test */
                    it(`reads PREMIUM USER Settings`, (done) => {
                        Client
                            .authorize({isPremium: true})
                            .then((Client) => {
                                Client.get(`/users/${Client.$user.$id}/settings`)
                                    .then(response => {
                                        let keys = Object.keys(response.data);
                                        let expectedKeys = [
                                            'userId', 'sex', 'birthDate', 'height', 'heightUnit',
                                            'heightCm', 'weightStart', 'weightStartUnit', 'weightStartKg',
                                            'weightCurrent', 'weightCurrentUnit', 'weightCurrentKg', 'weightTarget',
                                            'weightTargetUnit', 'weightTargetKg', 'age', 'bmr'
                                        ];

                                        // expectations
                                        expect(response.status).to.equal(200);
                                        expectedKeys.forEach((expectedKey) => {
                                            expect(keys).to.include(expectedKey);
                                        })
                                    })
                                    .catch((err) => {
                                        throw new Error(err.response.data.message)
                                    })
                                    .then(done, done);
                            })
                    });
                }
            });

            // Update user settings

            describe('Update User Settings', () => {

                /**
                 * this will help to avoid data duplication
                 */
                beforeEach(() => {
                    payload = {
                        birthDate: momentRandom().format('YYYY-MM-DD'),
                        height: _.random(120, 219),
                        heightUnit: 'CM',
                        sex: _.random(1, 2),
                        weightStart: _.random(40, 500),
                        weightStartUnit: 'KG',
                        weightCurrent: _.random(40, 500),
                        weightCurrentUnit: 'KG',
                        weightTarget: _.random(40, 500),
                        weightTargetUnit: 'KG',
                    }
                });


                /** test */
                it(`updates REGULAR USER settings`, (done) => {

                    Client
                        .authorize()
                        .then((Client) => {
                            Client.put(`/users/${Client.$user.$id}/settings`, payload)
                                .then(response => {
                                    // expectations
                                    Object.keys(payload).forEach((key) => {
                                        expect(payload[key]).to.equal(response.data[key]);
                                    });
                                })
                                .catch((err) => {
                                    throw new Error(err.response.data.message)
                                })
                                .then(done, done);
                        });
                });

                if (localeData.PREMIUM_AVAILABLE) {
                    /* test */
                    it(`PREMIUM USER Settings`, (done) => {
                        Client
                            .authorize({isPremium: true})
                            .then((Client) => {
                                Client.put(`/users/${Client.$user.$id}/settings`, payload)
                                    .then(response => {
                                        // expectations
                                        Object.keys(payload).forEach((key) => {
                                            expect(payload[key]).to.equal(response.data[key]);
                                        });
                                    })
                                    .catch((err) => {
                                        throw new Error(err.response.data.message)
                                    })
                                    .then(done, done);
                            })
                    });
                }
            });
        });
    });
});