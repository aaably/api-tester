export default {
    DEBUG: false,

    HTTP: {
        SCHEME: 'http://',
        HOST: 'mas.fitatu.com:8002',
        DEBUG_SUFFIX: '/app_dev.php',
        URI_PREFIX: '/api'
    },

    HEADERS: {
        'Content-Type': 'application/json',
        'API-Key': 'FITATU-MOBILE-APP',
        'API-Secret': 'PYRXtfs88UDJMuCCrNpLV',
        'APP-OS': 'ANDROID',
    },

    API: {
        LOGIN: '/login'
    },

    DEFAULT_LOCALE: 'FR',

    LOCALES: {
        FR: {
            LOCALE: 'fr_FR',
            PREMIUM_AVAILABLE: true,
            USERS: {
                REGULAR: {
                    LOGIN: 'fr-planner-r',
                    PASSWORD: 'f'
                },
                PREMIUM: {
                    LOGIN: 'baz2fr',
                    PASSWORD: 'f'
                },
            }
        }
    }
}