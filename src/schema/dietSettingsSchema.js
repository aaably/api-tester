export default {
    'type': 'object',
    'required': ['locale', 'nextAt', 'from', 'to', 'isReviewed', 'remainingDates'],
    'properties': {
        'locale' : {
            'type': 'string'
        },
        'nextAt': {
            'type': 'string',
            'format': 'date-time'
        },
        'from': {
            'type': 'string',
            'format': 'date-time'
        },
        'to': {
            'type': 'string',
            'format': 'date-time'
        },
        'isReviewed': {
            'type': 'boolean'
        },
        'remainingDates': {
            'type': 'object',
            'patternProperties': {
                '^(0[1-9]|[12][0-9]|3[01])-(0[1-9]|1[12])-20([1-9][0-9])$': {
                    'type': 'object',
                    'properties': {
                        'breakfast': {
                            '$ref': '#/definitions/mealSettings'
                        },
                        'second_breakfast': {
                            '$ref': '#/definitions/mealSettings'
                        },
                        'lunch': {
                            '$ref': '#/definitions/mealSettings'
                        },
                        'dinner': {
                            '$ref': '#/definitions/mealSettings'
                        },
                        'supper': {
                            '$ref': '#/definitions/mealSettings'
                        }
                    }
                }
            }
        }
    },
    'definitions': {
        'mealSettings': {
            'type': 'object',
            'properties': {
                'key': {
                    'type': 'string'
                },
                'name': {
                    'type': 'string'
                },
                'time': {
                    'type': 'string'
                }
            }
        }
    }
};