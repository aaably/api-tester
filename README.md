# TestSuite

## Instalacja

1. Sklonuj repo, przejdź do folderu głównego, odpal `npm install`

## Konfiguracja
1. Skopiuj plik *__config/config_dist.js__* do *__config/config.js__*
2. Uzupełnij brakujące pola w konfiguracji (plik *__config/config.js__*):
```
{
    DEBUG: false,

    HTTP: {
        SCHEME: 'http://',
        HOST: 'mas.fitatu.com:8002',
        DEBUG_SUFFIX: '/app_dev.php',
        URI_PREFIX: '/api'
    },

    HEADERS: {
        'Content-Type': 'application/json',
        'API-Key': 'FITATU-MOBILE-APP',
        'API-Secret': 'PYRXtfs88UDJMuCCrNpLV',
        'APP-OS': 'ANDROID',
    },

    API: {
        LOGIN: '/login'
    },

    DEFAULT_LOCALE: 'FR',

    LOCALES: {
        FR: {
            LOCALE: 'fr_FR',
            PREMIUM_AVAILABLE: true,
            USERS: {
                REGULAR: {
                    LOGIN: '',
                    PASSWORD: ''
                },
                PREMIUM: {
                    LOGIN: '',
                    PASSWORD: ''
                },
            }
        }
        
        // koleje języki
    }
}
```
	
## Użycie
1. Aby jednorazowo odpalić wszystkie testy przejdź w konsoli do katalogu głównego projektu i wpisz komende `npm run test`
2. Aby odpalić testy cykliczne przejdź w konsoli do katalogu głównego projektu i wpisz komendę `npm run prod`
3. Aby jednorazowo odpalić jeden test (przydatne przy testowaniu działania nowo tworzonego testu) odpal
     `npm run single <opis testu>`  
     
     Przykład:
     Definicja testu w pliku
     ```javascript
     describe('Billing plans', () => {
       ...    
     });
    ```
    
    `npm run single Billing` 

4. Lista dostępnych opcji:
    `npm run help` - wyświetla pomoc;
    `npm run watch` - odpala testy w trybie developerskim z listenerem na plikach;
    

## Tworzenie nowych testów
1. Szablon testu znajdziesz w *__example.test.js__* w głównym katalogu projektu
2. Aby dodać nowy test:
  - skopiuj ten plik do katalogu *__tests__* (struktura wewnątrz modules nie ma znaczenia)
  - zmień nazwę pliku na dowolną inną (upewnij się tylko że plik kończy się rozszerzeniem *__.test.js__*
  - wyedytuj odpowiednio treść pliku
2. Nowo utworzone testy MUSZĄ znaleźć się w folderze tests (struktura wewnątrz modules nie ma znaczenia)
3. Do pisania testów używamy __*chai*__  http://chaijs.com/

## Dodatkowe
zmiana poziomu zabezpieczeń dla Gmaila: https://www.google.com/settings/u/1/security/lesssecureapps