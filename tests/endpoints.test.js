import {expect} from 'chai'
import endpoints from '../config/endpoints'
import CONFIG from '../config/config'
import ApiClient from '../src/ApiClient'

describe('Endpoints checker', () => {
    Object.keys(CONFIG.LOCALES).forEach(locale => {
        describe(`Testing locale ${locale}`, () => {
            let client = new ApiClient({
                locale
            })

            Object.keys(endpoints).forEach((key) => {
                it(`it checks ${key} endpoint`, function(done) {

                    let endpoint = endpoints[key]
                    let methodsCount = endpoint.METHODS.length
                    this.timeout(endpoint.TIMEOUT ? endpoint.TIMEOUT : 4000);

                    if (endpoint.SKIP){
                        return this.skip();
                    }

                    endpoint.ALLOWED_STATUS_CODES = [...new Set([...endpoint.ALLOWED_STATUS_CODES, ...[401, 500]])]

                    endpoint.METHODS.forEach((method, index) => {
                        let finish = () => {};
                        if (index + 1 == methodsCount){
                            finish = done
                        }
                        client[method.toLowerCase()](endpoint.URL, {}).then(response => {
                            // if success do nth
                        }).catch(error => {
                            expect(endpoint.ALLOWED_STATUS_CODES).to.include(error.request.res.statusCode)
                        }).then(finish, finish)
                    });
                });
            });
        });
    });
})
