import chai, {expect} from 'chai';
import chaiJSONSchema from 'chai-json-schema';
import CONFIG from './../../../config/config';
import ApiClient from './../../../src/ApiClient';
import schema from './../../../src/schema/billingPlanSchema';

chai.use(chaiJSONSchema)

describe('Billing plans', () => {

    Object.keys(CONFIG.LOCALES).forEach(locale => {
        let localeData = CONFIG.LOCALES[locale];

        if (localeData.PREMIUM_AVAILABLE) {

            describe(locale, () => {
                let Client = new ApiClient({locale});

                /* test */
                it('display billing plans', function (done) {

                    Client
                        .authorize()
                        .then((Client) => {
                            Client.get(`/billing/plans?locale=${localeData.LOCALE}`)
                                .then(response => {
                                    // expectations
                                    expect(response.status).to.equal(200);
                                    response.data.forEach((plan) => {
                                        expect(plan).to.be.jsonSchema(schema);
                                    })
                                })
                                .catch((err) => {
                                    throw new Error(err.response.data.message)
                                })
                                .then(done, done);
                        })
                });
            });
        }
    });
});