import CONFIG from './../config/config'
import axios from 'axios'
import User from './User'

/**
 * @author    Sebastian Szczepański
 * @copyright Fitatu Sp. z o.o.
 */
export default class ApiClient {
    /**
     *
     * @param locale
     */
    constructor(options = {}) {
        this.$locale = options.locale || CONFIG.DEFAULT_LOCALE;
        this.$localeData = CONFIG.LOCALES[this.$locale];
        this.$http = axios.create({
            baseURL: this.getUrl()
        });

        this.setDefaultHeaders();
    }

    setDefaultHeaders() {
        Object.keys(CONFIG.HEADERS).forEach(key => {
            this.$http.defaults.headers.common[key] = CONFIG.HEADERS[key]
        })
    }

    /**
     * Login user by given type
     * @param type
     * @returns {*}
     */
    login(type){
        let isPremium = type == 'PREMIUM'
        let user = new User({
            locale: this.$locale,
            isPremium
        })

        return this.post(CONFIG.API.LOGIN, user.getCredentialsAsPayload())
    }

    /**
     * Authorize requests
     * @param options
     * @returns {Promise}
     */
    authorize(options = {}){
        let isPremium = options.isPremium || false

        this.$user = new User({
            locale: this.$locale,
            isPremium
        })

        return new Promise((resolve, reject) => {
            return this.post(CONFIG.API.LOGIN, this.$user.getCredentialsAsPayload()).then(({data}) => {
                this.$user.extractToken(data.token);

                this.$http.defaults.headers.common['Authorization'] = 'Bearer ' + data.token;

                resolve(this);
            });
        });
    }

    /**
     *
     * @returns {string}
     */
    getLocaleSlug() {
        return this.$localeData.LOCALE.toString().toLowerCase().replace('_', '-') +'.';
    }

    /**
     *
     * @param suffix
     * @returns {string}
     */
    getUrl(uri = '') {
        let url = CONFIG.HTTP.SCHEME + this.getLocaleSlug() + CONFIG.HTTP.HOST

        if (CONFIG.DEBUG) {
            url = url + CONFIG.HTTP.DEBUG_SUFFIX;
        }

        return url + CONFIG.HTTP.URI_PREFIX + uri;
    }

    /**
     * Check if set locale has premium available
     * @returns {boolean}
     */
    hasPremium(){
        return this.$localeData.PREMIUM_AVAILABLE;
    }

    /**
     * Request GET
     * @param uri
     * @returns {AxiosPromise<any>}
     */
    get(uri){
        return this.$http.get(uri)
    }

    /**
     * Request POST
     * @param uri
     * @param payload
     * @returns {AxiosPromise<any>}
     */
    post(uri, payload){
        return this.$http.post(uri, payload)
    }

    /**
     * Request PATCH
     * @param uri
     * @param payload
     * @returns {AxiosPromise<any>}
     */
    patch(uri, payload){
        return this.$http.patch(uri, payload)
    }

    /**
     * Request PUT
     * @param uri
     * @param payload
     * @returns {AxiosPromise<any>}
     */
    put(uri, payload){
        return this.$http.put(uri, payload)
    }

    /**
     * Request DELETE
     * @param uri
     * @param payload
     * @returns {AxiosPromise}
     */
    delete(uri, payload){
        return this.$http.delete(uri)
    }
}