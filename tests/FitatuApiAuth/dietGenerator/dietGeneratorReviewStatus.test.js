import chai, {expect} from 'chai';
import chaiJSONSchema from 'chai-json-schema';
import CONFIG from '../../../config/config';
import ApiClient from '../../../src/ApiClient';
import schema from '../../../src/schema/dietGeneratorStatusSchema';
chai.use(chaiJSONSchema)

describe('Diet Generator Settings Review status', () => {

    Object.keys(CONFIG.LOCALES).forEach(locale => {
        let localeData = CONFIG.LOCALES[locale];

        describe(locale, () => {
            let Client = new ApiClient({locale});

            /* test */
            it(`Checks review status for REGULAR USER`, (done) => {

                Client
                    .authorize()
                    .then((Client) => {
                        Client.get(`/diet-generator/${Client.$user.$id}/status`)
                            .then(response => {
                                // expectations
                                throw new Error('Regular user should not be allowed to view DietGenerator Settings')
                            })
                            .catch((err) => {
                                expect(err.request.res.statusCode).to.equal(404);
                            })
                            .then(done, done);
                    })
            });

            if (localeData.PREMIUM_AVAILABLE) {
                /* test */
                it(`Checks review status for PREMIUM USER`, (done) => {
                    Client
                        .authorize({isPremium: true})
                        .then((Client) => {
                            Client.get(`/diet-generator/${Client.$user.$id}/status`)
                                .then(response => {
                                    //expectations
                                    expect(response.status).to.equal(200);
                                    expect(response.data).to.be.jsonSchema(schema);
                                })
                                .catch((err) => {
                                    throw new Error(err.response.data.message)
                                })
                                .then(done, done);
                        })
                });
            }
        });
    });
});