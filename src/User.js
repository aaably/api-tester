import ApiClient from './ApiClient'
import CONFIG from './../config/config'
import jwtDecode from 'jwt-decode'

/**
 * @author    Sebastian Szczepański
 * @copyright Fitatu Sp. z o.o.
 */
export default class User {
    constructor(options = {}) {
        this.$isPremium = options.isPremium
        this.$locale = options.locale
        this.setCredentials()
    }

    /**
     * @returns {User}
     */
    setCredentials() {
        this.$credentials = CONFIG.LOCALES[this.$locale].USERS.REGULAR

        if (this.$isPremium && !CONFIG.LOCALES[this.$locale].PREMIUM_AVAILABLE) {
            throw "Provided locale has no premium services available";
        }

        if (this.$isPremium) {
            this.$credentials = CONFIG.LOCALES[this.$locale].USERS.PREMIUM
        }

        return this
    }

    /**
     * @returns {{_username: (string|string|string), _password: string}}
     */
    getCredentialsAsPayload() {
        return {
            _username: this.$credentials.LOGIN,
            _password: this.$credentials.PASSWORD
        }
    }

    /**
     * Set user data from token
     * @param token
     */
    extractToken(token){
        let tokenData = jwtDecode(token);
        this.$token = token;

        this.$id = tokenData.id
        this.$roles = tokenData.roles
        this.$username = tokenData.username
    }
}