export default {
    'type': 'object',
    'required': [
        'userId', 'energy', 'activityBase', 'activityTraining', 'activityEnergyInclusionMode', 'weightChangeSpeed',
        'weightChangeSpeedUnit', 'weightChangeDirection', 'manualEnergyTarget', 'proteinPercentage', 'proteinWeight',
        'fatPercentage', 'fatWeight', 'carbohydratePercentage', 'carbohydrateWeight', 'mealSchema', 'meatlessFridays',
        'excludedProducts', 'mealSchemaPreferences', 'TMR', 'calculatedEnergy'
    ],
    'properties': {
        'userId': {
            'type': 'string'
        },
        'energy': {
            'type': 'number'
        },
        'activityBase': {
            'enum': [1.4, 1.5, 1.7, 2]
        },
        'activityTraining': {
            'enum': [1.4, 1.5, 1.7, 2]
        },
        'activityEnergyInclusionMode': {
            'enum': [
                'IGNORE_ENTIRELY_ACTIVITY_ENERGY',
                'INCREASE_ENERGY_BY_ACTIVITY_TRAINING',
                'INCLUDE_ADDED_ACTIVITIES_IN_ENERGY',
                'INCLUDE_ADDED_ACTIVITIES_AND_INCREASE_ENERGY_BY_ACTIVITY_TRAINING'
            ]
        },
        'weightChangeSpeed': {
            'type': 'number',
            'minimum': 0.1,
            'maximum': 1,
            'multipleOf': 0.1
        },
        'weightChangeSpeedUnit': {
            'enum': ['KG', 'ST_LB', 'LB']
        },
        'weightChangeDirection': {
            'enum': [-1, 0, 1]
        },
        'manualEnergyTarget': {
            'type': 'boolean'
        },
        'proteinPercentage': {
            'type': ['number', 'null'],
            'maximum': 100
        },
        'proteinWeight': {
            'type': ['number', 'null']
        },
        'fatPercentage': {
            'type': ['number', 'null'],
            'maximum': 100
        },
        'fatWeight': {
            'type': ['number', 'null']
        },
        'carbohydratePercentage': {
            'type': ['number', 'null'],
            'maximum': 100
        },
        'carbohydrateWeight': {
            'type': ['number', 'null']
        },
        'mealSchema': {
            'type': 'array'
        },
        'meatlessFridays': {
            'type': 'boolean'
        },
        'excludedProducts': {
            'type': 'array'
        },
        'mealSchemaPreferences': {
            'type': 'object',
            'properties': {
                'breakfast': {
                    '$ref': '#/definitions/mealPreference'
                },
                'second_breakfast': {
                    '$ref': '#/definitions/mealPreference'
                },
                'lunch': {
                    '$ref': '#/definitions/mealPreference'
                },
                'dinner': {
                    '$ref': '#/definitions/mealPreference'
                },
                'snack': {
                    '$ref': '#/definitions/mealPreference'
                },
                'supper': {
                    '$ref': '#/definitions/mealPreference'
                }
            }
        },
        'TMR': {
            'type': 'number'
        },
        'calculatedEnergy': {
            'type': 'number'
        },
        'dietGeneration': {
            'type': 'object',
            'properties': {
                'timetable': {
                    'type': 'object',
                    'properties': {
                        'from': {
                            'type': 'string'
                        },
                        'to': {
                            'type': 'string'
                        },
                        'nextAt': {
                            'type': 'string'
                        }
                    }
                }
            }
        }
    },
    'definitions': {
        'mealPreference': {
            'type': 'object',
            'properties': {
                'selected': {
                    'type': 'boolean'
                },
                'mealName': {
                    'type': 'string'
                },
                'mealTime': {
                    'type': 'string'
                },
                'items': {
                    'type': 'array'
                },
                'preparationTime': {
                    'type': 'number'
                },
                'coldDish': {
                    'type': 'boolean'
                },
                'warmDish': {
                    'type': 'boolean'
                },
                'reheatDish': {
                    'type': 'boolean'
                }
            }
        }
    }
};