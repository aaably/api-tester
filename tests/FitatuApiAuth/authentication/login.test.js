import {expect} from 'chai';
import CONFIG from '../../../config/config';
import ApiClient from '../../../src/ApiClient';
import User from '../../../src/User';

describe('Authentication', () => {

    Object.keys(CONFIG.LOCALES).forEach(locale => {
        describe(locale, () => {
            let Client = new ApiClient({locale});

            Object.keys(CONFIG.LOCALES[locale].USERS).forEach((userType) => {
                /* test */
                it(`login ${userType} USER`, function (done) {
                    Client.login(userType)
                        .then(({data}) => {
                            // expectations
                            expect(data.token).to.be.a('string')

                        }).catch((err) => {
                            throw new Error(err.response.data.message)
                    }).then(done, done)
                });
            });

            /* test */
            it('login DEMO USER', (done) => {
                Client.post('login-demo', {locale: CONFIG.LOCALES[locale].LOCALE})
                    .then(response => {
                        // expectations
                        expect(response.status).to.equal(201);
                        expect(response.data.token).to.be.a('string');
                        expect(response.data.demoUser).to.have.property('password').that.is.a('string');
                        expect(Object.keys(response.data)).to.have.lengthOf(2);

                    }).catch((err) => {
                        throw new Error(err.response.data.message)
                }).then(done, done)
            });
        });
    });
});