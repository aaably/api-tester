export default {
    // Fitatu API Auth
    "login": {
        URL: "/login",
        METHODS: ['POST'],
        ALLOWED_STATUS_CODES: [422]
    },
    
    "login demo": {
        URL: "/login-demo",
        METHODS: ['POST'],
        ALLOWED_STATUS_CODES: [400]
    },
    
    "login internal": {
        URL: "/login-internal",
        METHODS: ['POST'],
        ALLOWED_STATUS_CODES: [400],
        // SKIP: true,
    },
    
    "roles hierarchy": {
        URL: "/roles/hierarchy",
        METHODS: ['GET'],
        ALLOWED_STATUS_CODES: [400]
    },

    "reset password": {
        URL: "/users/action/reset-password",
        METHODS: ['POST'],
        ALLOWED_STATUS_CODES: [400, 404], // endpoint do poprawy - zły status
        PAYLOAD: {
            password: 'test',
            token: 'test'
        }
    },

    "send email for reset pass": {
        URL: "/users/action/reset-password/send-email",
        METHODS: ['POST'],
        ALLOWED_STATUS_CODES: [400, 404] // endpoint do poprawy - zły status
    },
    
    "reset password token check": {
        URL: "/users/action/reset-password/token/1234123123",
        METHODS: ['GET'],
        ALLOWED_STATUS_CODES: [400, 404]
    },
    
    "billing locales": {
        URL: "/billing/locales",
        METHODS: ['GET'],
        ALLOWED_STATUS_CODES: [],
    },

    "billing plans": {
        URL: "/billing/plans",
        METHODS: ['GET'],
        ALLOWED_STATUS_CODES: []
    },

    "billing plan": {
        URL: "/billing/plans/plan-id",
        METHODS: ['GET'],
        ALLOWED_STATUS_CODES: []
    },

    "billing callbacks": {
        URL: "/billing/1/callback",
        METHODS: ['POST'],
        ALLOWED_STATUS_CODES: [403]
    },

    "billing log": {
        URL: "/billing/1/log",
        METHODS: ['POST'],
        ALLOWED_STATUS_CODES: []
    },

    "diet generator settings": {
        URL: "/diet-generator/123/settings",
        METHODS: ['GET', 'PATCH'],
        ALLOWED_STATUS_CODES: []
    },

    "diet generator status": {
        URL: "/diet-generator/123/status",
        METHODS: ['GET'],
        ALLOWED_STATUS_CODES: []
    },
    
    "errors": {
        URL: "/errors",
        METHODS: ['POST'],
        ALLOWED_STATUS_CODES: [400],
    },

    "likeables (product)": {
        URL: "/likeables/PRODUCT/1",
        METHODS: ['GET', 'PUT'],
        ALLOWED_STATUS_CODES: []
    },

    "likeables (recipe)": {
        URL: "/likeables/RECIPE/1",
        METHODS: ['GET', 'PUT'],
        ALLOWED_STATUS_CODES: []
    },
    "measurements": {
        URL: "/users/1/measurements",
        METHODS: ['POST'],
        ALLOWED_STATUS_CODES: []
    },

    "measurements chart weight": {
        URL: "/users/1/measurements/chart/weight",
        METHODS: ['GET'],
        ALLOWED_STATUS_CODES: []
    },
    
    "measurements body part": {
        URL: "/users/1/measurements/size/neck",
        METHODS: ['GET'],
        ALLOWED_STATUS_CODES: []
    },

    "measurements summary weight": {
        URL: "/users/1/measurements/summary/size",
        METHODS: ['GET'],
        ALLOWED_STATUS_CODES: []
    },

    "measurements for date": {
        URL: "/users/1/measurements/2017-12-23",
        METHODS: ['DELETE', 'GET', 'PUT'],
        ALLOWED_STATUS_CODES: []
    },

    "norms": {
        URL: "/users/1/norms/day",
        METHODS: ['GET'],
        ALLOWED_STATUS_CODES: [404] // endpoint do poprawy - w body zrzuca 401 a status code jest 404
    },
    
    "ping": {
        URL: "/ping",
        METHODS: ['GET'],
        ALLOWED_STATUS_CODES: []
    },

    "reviews": {
        URL: "/reviews/diet",
        METHODS: ['POST', 'GET', 'PATCH', 'PUT'],
        ALLOWED_STATUS_CODES: []
    },

    "diet plan settings": {
        URL: "/diet-plan/1/settings",
        METHODS: ['GET', 'PUT'],
        ALLOWED_STATUS_CODES: []
    },

    "exclusions": {
        URL: "/diet-plan/1/settings/preferences/exclusions",
        METHODS: ['GET', 'PUT'],
        ALLOWED_STATUS_CODES: []
    },

    "limits": {
        URL: "/diet-plan/1/settings/preferences/limits",
        METHODS: ['GET', 'PATCH'],
        ALLOWED_STATUS_CODES: []
    },

    "meal schema preferences": {
        URL: "/diet-plan/1/settings/preferences/meal-schema",
        METHODS: ['GET', 'POST'],
        ALLOWED_STATUS_CODES: []
    },

    "diet plan day settings": {
        URL: "/diet-plan/1/settings/2017-07-11",
        METHODS: ['GET'],
        ALLOWED_STATUS_CODES: [],
        TIMEOUT: 5000
    },

    "create user": {
        URL: "/users",
        METHODS: ['POST'],
        ALLOWED_STATUS_CODES: [400]
    },

    "users": {
        URL: "/users/1",
        METHODS: ['DELETE', 'GET', 'PATCH'],
        ALLOWED_STATUS_CODES: []
    },

    "activate token": {
        URL: "/users/1/action/activate/12312312",
        METHODS: ['PUT'],
        ALLOWED_STATUS_CODES: [409]
    },

    "change password": {
        URL: "/users/1/action/change-password",
        METHODS: ['PUT'],
        ALLOWED_STATUS_CODES: []
    },

    // Fitatu Api Planner
    
    "activity": {
        URL: "/users/1/activities/123",
        METHODS: ['GET'],
        ALLOWED_STATUS_CODES: []
    },
    
    "activity plan custom": {
        URL: "/activity-plan/1/custom",
        METHODS: ['POST'],
        ALLOWED_STATUS_CODES: []
    },
    
    "activity plan day": {
        URL: "/activity-plan/1/day/2017-08-08",
        METHODS: ['GET', 'POST'],
        ALLOWED_STATUS_CODES: []
    },

    "copy activity plan": {
        URL: "/activity-plan/1/day/2017-08-08/action/copy-to",
        METHODS: ['PUT'],
        ALLOWED_STATUS_CODES: []
    },    
    
    "activity plan day custom": {
        URL: "/activity-plan/1/day/2017-10-10/custom",
        METHODS: ['POST'],
        ALLOWED_STATUS_CODES: []
    },
    
    "activity plan day diet item": {
        URL: "/activity-plan/1/day/2017-07-07/1",
        METHODS: ['GET', 'DELETE', "PUT"],
        ALLOWED_STATUS_CODES: []
    },
        
    "activity plan summary": {
        URL: "/activity-plan/1/summary/day/2017-07-10",
        METHODS: ['GET'],
        ALLOWED_STATUS_CODES: []
    },
    
    "diet plan day": {
        URL: "/diet-plan/1/day/2017-10-10",
        METHODS: ['GET'],
        ALLOWED_STATUS_CODES: []
    },
    "diet plan copy": {
        URL: "/diet-plan/1/day/2017-10-10/action/copy-to",
        METHODS: ['GET'],
        ALLOWED_STATUS_CODES: []
    },
    "diet plan day meal": {
        URL: "/diet-plan/1/day/2017-10-10/breakfast",
        METHODS: ['PUT'],
        ALLOWED_STATUS_CODES: [405]
    },
    
    "diet plan day diet item": {
        URL: "/diet-plan/1/day/2017-07-10/breakfast/1",
        METHODS: ['DELETE', 'GET', "PUT"],
        ALLOWED_STATUS_CODES: []
    },
    
    "diet plan export": {
        URL: "/diet-plan/1/export",
        METHODS: ['GET'],
        ALLOWED_STATUS_CODES: []
    },
    
    "progress bar": {
        URL: "/diet-plan/1/progress-bar/2017-07-10",
        METHODS: ['GET'],
        ALLOWED_STATUS_CODES: []
    },
    
    "diet plan summary": {
        URL: "/diet-plan/1/summary/day/2017-07-10",
        METHODS: ['GET'],
        ALLOWED_STATUS_CODES: []
    },
        
    "activity plan for dates": {
        URL: "/activity-plan/1/from/2017-07-10/to/2017-07-12",
        METHODS: ['GET'],
        ALLOWED_STATUS_CODES: []
    },
    
    "diet and activity plan": {
        URL: "/diet-and-activity-plan/1/day/2017-07-10",
        METHODS: ['GET'],
        ALLOWED_STATUS_CODES: []
    },
    
    "create product": {
        URL: "/products",
        METHODS: ['POST'],
        ALLOWED_STATUS_CODES: []
    },
    
    "product categories": {
        URL: "/products/categories",
        METHODS: ['GET'],
        ALLOWED_STATUS_CODES: []
    },
    
    "category measures": {
        URL: "/products/measures/1",
        METHODS: ['GET'],
        ALLOWED_STATUS_CODES: []
    },

    "product": {
        URL: "/products/1",
        METHODS: ['DELETE', 'GET', 'PATCH', 'PUT'],
        ALLOWED_STATUS_CODES: []
    },

    "create product barcode": {
        URL: "/products/1/barcodes",
        METHODS: ['POST'],
        ALLOWED_STATUS_CODES: []
    },

    "get product nutrients by measure quantity": {
        URL: "/products/1/nutrients/measure/1/1",
        METHODS: ['GET'],
        ALLOWED_STATUS_CODES: []
    },

    "get product nutrients by measure weight": {
        URL: "products/1/nutrients/weight/1",
        METHODS: ['GET'],
        ALLOWED_STATUS_CODES: []
    },

    "create recipe": {
        URL: "/recipes",
        METHODS: ['POST'],
        ALLOWED_STATUS_CODES: []
    },

    "recipes and user action": {
        URL: "/recipes-and-user-action/1/1",
        METHODS: ['GET'],
        ALLOWED_STATUS_CODES: []
    },

    "recipes categories": {
        URL: "/recipes/categories",
        METHODS: ['GET'],
        ALLOWED_STATUS_CODES: []
    },

    "recipe": {
        URL: "/recipes/1",
        METHODS: ['DELETE', 'GET', 'PUT'],
        ALLOWED_STATUS_CODES: []
    },

    "shopping list": {
        URL: "/shopping-list/1",
        METHODS: ['GET'],
        ALLOWED_STATUS_CODES: []
    },
    
    // Fitatu API Planner Resources
    "generator resources - food": {
        URL: "/resources/generator/food",
        METHODS: ['GET'],
        ALLOWED_STATUS_CODES: [400]
    },
    
    "generator products": {
        URL: "/resources/generator/products",
        METHODS: ['GET'],
        ALLOWED_STATUS_CODES: [400]
    },
    
    "generator recipes": {
        URL: "/resources/generator/recipes",
        METHODS: ['GET'],
        ALLOWED_STATUS_CODES: [400]
    },

    "update mesures": {
        URL: "/measures/actions/update-all",
        METHODS: ['PUT'],
        ALLOWED_STATUS_CODES: []
    },
    
    "resources measures": {
        URL: "/resources/measures",
        METHODS: ['GET'],
        ALLOWED_STATUS_CODES: []
    },
        
    "entity media": {
        URL: "/resources/media/product/1",
        METHODS: ['GET', 'POST'],
        ALLOWED_STATUS_CODES: []
    },
    
    "media": {
        URL: "/resources/media/1579f332-c4c5-11e7-abc4-cec278b6b50a",
        METHODS: ['GET'],
        ALLOWED_STATUS_CODES: []
    },
        
    "report activites": {
        URL: "/activities/1/action/report",
        METHODS: ['POST'],
        ALLOWED_STATUS_CODES: []
    },
        
    "report product": {
        URL: "/products/1/action/report",
        METHODS: ['POST'],
        ALLOWED_STATUS_CODES: []
    },
        
    "report recipe": {
        URL: "/recipes/1/action/report",
        METHODS: ['POST'],
        ALLOWED_STATUS_CODES: []
    },
        
    "categories": {
        URL: "/resources/products/categories",
        METHODS: ['GET'],
        ALLOWED_STATUS_CODES: []
    },

    "child categories": {
        URL: "/resources/products/categories/1",
        METHODS: ['GET'],
        ALLOWED_STATUS_CODES: []
    },

    "recommended measures for product": {
        URL: "/resources/measures/propositions/1/test/1.25/1.23",
        METHODS: ['GET'],
        ALLOWED_STATUS_CODES: []
    },
    
    "resources products": {
        URL: "/resources/products",
        METHODS: ['GET'],
        ALLOWED_STATUS_CODES: []
    },
        
    "reources product": {
        URL: "/resources/products/1",
        METHODS: ['GET'],
        ALLOWED_STATUS_CODES: [400]
    },
    
    "reources recipes": {
        URL: "/resources/recipes/1",
        METHODS: ['GET'],
        ALLOWED_STATUS_CODES: [400]
    },

    //    Fitatu API Search
    "search activities": {
        URL: "/activities",
        METHODS: ['GET'],
        ALLOWED_STATUS_CODES: []
    },

    "search activities phrase": {
        URL: "/search/activity/jeden",
        METHODS: ['GET'],
        ALLOWED_STATUS_CODES: []
    },

    "last used activities": {
        URL: "/search/activity/1/last-used",
        METHODS: ['GET'],
        ALLOWED_STATUS_CODES: []
    },
    
    "search brand": {
        URL: "/search/brands/cola",
        METHODS: ['GET'],
        ALLOWED_STATUS_CODES: []
    },
    
    "search food for user": {
        URL: "/search/food/user/1",
        METHODS: ['GET'],
        ALLOWED_STATUS_CODES: []
    },

    "last used food": {
        URL: "/search/food/user/1/last-used",
        METHODS: ['GET'],
        ALLOWED_STATUS_CODES: []
    },
    
    "search food": {
        URL: "/search/food/pom",
        METHODS: ['GET'],
        ALLOWED_STATUS_CODES: []
    },
        
    "last used": {
        URL: "/search/food/1/last-used",
        METHODS: ['GET'],
        ALLOWED_STATUS_CODES: []
    },
    
    "new search": {
        URL: "/search/new/food",
        METHODS: ['GET'],
        ALLOWED_STATUS_CODES: []
    },

}