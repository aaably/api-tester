export default {
    'type': 'object',
    'required': ['selected', 'required', 'limits', 'types', 'options'],
    'properties': {
        'selected': {
            'type': 'object',
            'required': ['timeId', 'typeIds'],
            'properties': {
                'timeId': {
                    'type': 'number'
                },
                'typeIds': {
                    'type': 'array'
                }
            }
        },
        'required': {
            'type': 'object',
            'required': ['timeId', 'typeIds'],
            'properties': {
                'timeId': {
                    'type': 'number'
                },
                'typeIds': {
                    'type': 'array'
                }
            }
        },
        'limits': {
            'type': 'object',
            'anyOf': [
                {'required': ['1'] },
                {'required': ['2'] },
                {'required': ['3'] }
            ],
            'properties': {
                '1': { 'type': 'object' },
                '2': { 'type': 'object' },
                '3': { 'type': 'object' }
            }
        },
        'types': {
            'type': 'object'
        },
        'options': {
            'oneOf': [
                {'type': 'object'},
                {'type': 'array'}
            ]
        }
    }
};